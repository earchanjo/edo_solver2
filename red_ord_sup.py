import numpy as np 
import pandas as pd
import matplotlib.pyplot as plt

from math import *

class Edo_Solver():
    def __init__(self) -> None:
        self.F_matrix = []
        self.r_matrix = []

    def F_matrix_func(self,an,t):
        n_ord = len(an) - 1
        F = np.zeros(shape=(n_ord,n_ord))
        #an = input("Entre com os termos da equação (ans) separados por virgula (,): ")

        t = t
        for i in range(len(an)):
            an[i] = eval(str(an[i]))

        for i in range(n_ord):
            if i == n_ord - 1 :
                for j in range(n_ord):
                    F[i][j] = -an[n_ord-j]/an[0]
            else:
                F[i][i+1] = 1.0
        self.F_matrix =  F
        #return self.F_matrix

    def solve(self,Y_0,h,niter,an,st=0.0):
        Y_i = np.zeros(shape=(niter+1,len(Y_0)))
        Y_i[0] = Y_0

        k_1 = np.array([])
        k_2 = np.array([])
        k_3 = np.array([])
        k_4 = np.array([])

        T_s =  np.zeros(len(Y_0))

        t = 0
        t_vector = [0]

        self.F_matrix_func(an,t)
        if st != 0.0:
            T_s[-1] =  eval(str(st))/ eval(str(an[0]))

        for i in range(0, niter):
            k_1 = self.mult_axb(self.F_matrix,Y_i[i]) + T_s
            Y_k2 = Y_i[i] + (h/2)*k_1
            k_2 = self.mult_axb(self.F_matrix,Y_k2) + T_s
            y_k3 = Y_i[i] + (h/2)* k_2
            k_3 = self.mult_axb(self.F_matrix,y_k3) + T_s
            y_k4 = Y_i[i] + h*k_3
            k_4 = self.mult_axb(self.F_matrix,y_k4) + T_s
            
            Y_i[i+1] = Y_i[i] + (h/6)*(k_1 + 2*k_2 + 2*k_3 + k_4)

            t += h
            t_vector.append(t)

            self.F_matrix_func(an, t)
            T_s[-1] = eval(str(st))/ eval(str(an[0]))
            print(T_s)
            
        data = pd.DataFrame(t_vector)
        data2 = pd.DataFrame(Y_i)
        data = pd.concat([data, data2], axis =1)
        data.to_csv("arquivo.csv")

        print(data)
        return data

    def mult_axb(self,a,b):
        r = []
        for i in range(len(a)):
            a_1 = 0
            for j in range(len(b)):
                a_1 += a[i][j] * b[j]
            r.append(a_1)
            result = np.array(r)
        return result

    def plot(self,matriz):

        matrix = matriz.to_numpy().T
        
        for y in range(len(matrix)):
            if y > 0:
                plt.plot(matrix[0], matrix[y], label= "derivada: " + str(y-1))

        plt.title("Gráfico das derivadas")
        plt.grid()
        plt.xlabel("tempo")
        plt.ylabel("derivadas")
        plt.legend()
        plt.show()



def menu():
    while(True):
        try:
            print("\t\t    /*---------------------------------------------------------------------------*\\\n\
                    |            Equação Diferencial de Ordem Superior até 6ª ordem                   |\n\
                    \*---------------------------------------------------------------------------*/")

            dict_nord = {2:"Eq= a0P''(t) + a1P'(t) + a2P(t) =",
                        3:"Eq= a0P'''(t) + a1P''(t) + a2P'(t) + a3P(t) =",
                        4:"Eq= a0P''''(t) + a1P'''(t) + a2P''(t) + a3P'(t) + a4P(t) =",
                        5:"Eq= a0P'''''(t) + a1P''''(t) + a2P'''(t) + a3P''(t) + a4P'(t) + a5P(t) =",
                        6:"Eq= a0P''''''(t) + a1P'''''(t) + a2P''''(t) + a3P'''(t) + a4P''(t) + a5P'(t) + a6P(t) ="}
            
            nord = int(input("Entre com a ordem da equação:  "))

            resposta_tfs = input("A equação e homogenea?  S (sim) - N (nao):  ")
            if not resposta_tfs.isalpha():
                raise ValueError

            if resposta_tfs.lower() == "s":
                print(dict_nord[nord] + " 0")
            elif resposta_tfs.lower() == "n":
                print(dict_nord[nord] + " f(x)")

            print("Esta equacao está correta?" )
            resposta = input(" S (sim) - N (nao):  ")

            if not resposta.isalpha():
                raise ValueError

            

            if resposta.lower() == "s":
                print("Entre com os coeficientes em ordem mostrada na equacao:")
                ans = [input("a" + str(i) + ": ").format(name=i) for i in range(nord+1)]
                print(ans)

                st = 0
                
                if resposta_tfs.lower() == "n":
                    st = input("Entre com o termo fonte f(x):  ")
            
                y_0 = [float(input("Entre com as condicoes iniciais: ")) for i in range(nord)]
                n_iter = int(input("Tempo de simulacao:  "))
                h = float(input("O passo do algoritmo:  "))

                solver = Edo_Solver()
                resultado = solver.solve(y_0,h,n_iter,ans,st=st)
                #print(resultado)
                #resultado.plot()
                solver.plot(resultado)

            elif resposta.lower() == "n":
                print("Entre com o modelo novamente")
                continue

        except ValueError:
            print("Resposta incorreta. Entre com as opcoes validas")

menu()
